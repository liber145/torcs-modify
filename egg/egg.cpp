/***************************************************************************

    file                 : egg.cpp
    created              : Fri Jul 7 19:47:18 UTC 2017
    copyright            : (C) 2002 liyujun

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  * *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h> 
#include <string.h> 
#include <math.h>

#include <tgf.h> 
#include <track.h> 
#include <car.h> 
#include <raceman.h> 
#include <robottools.h>
#include <robot.h>

#include <unistd.h>
#include "sensors.h"

static Sensors *trackSens;
static float trackSensAngle[19];


static tTrack	*curTrack;

static void initTrack(int index, tTrack* track, void *carHandle, void **carParmHandle, tSituation *s); 
static void newrace(int index, tCarElt* car, tSituation *s); 
static void drive(int index, tCarElt* car, tSituation *s); 
static void endrace(int index, tCarElt *car, tSituation *s);
static void shutdown(int index);
static int  InitFuncPt(int index, void *pt); 


/* 
 * Module entry point  
 */ 
extern "C" int 
egg(tModInfo *modInfo) 
{
    memset(modInfo, 0, 10*sizeof(tModInfo));

    modInfo->name    = strdup("egg");		/* name of the module (short) */
    modInfo->desc    = strdup("");	/* description of the module (can be long) */
    modInfo->fctInit = InitFuncPt;		/* init function */
    modInfo->gfId    = ROB_IDENT;		/* supported framework version */
    modInfo->index   = 1;

    return 0; 
} 

/* Module interface initialization. */
static int 
InitFuncPt(int index, void *pt) 
{ 
    tRobotItf *itf  = (tRobotItf *)pt; 

    itf->rbNewTrack = initTrack; /* Give the robot the track view called */ 
				 /* for every track change or new race */ 
    itf->rbNewRace  = newrace; 	 /* Start a new race */
    itf->rbDrive    = drive;	 /* Drive during race */
    itf->rbPitCmd   = NULL;
    itf->rbEndRace  = endrace;	 /* End of the current race */
    itf->rbShutdown = shutdown;	 /* Called before the module is unloaded */
    itf->index      = index; 	 /* Index used if multiple interfaces */
    return 0; 
} 

/* Called for every track change or new race. */
static void  
initTrack(int index, tTrack* track, void *carHandle, void **carParmHandle, tSituation *s) 
{ 
    curTrack = track;
    *carParmHandle = NULL; 
} 

/* Start a new race. */
static void  
newrace(int index, tCarElt* car, tSituation *s) 
{ 
    for (int i=0; i<19; ++i)
    {
        trackSensAngle[i] = -90 + 10.0*i;
    }

    trackSens = new Sensors(car, 19);
    for (int i=0; i<19; ++i)
    {
        trackSens->setSensor(i, trackSensAngle[i], 200);
    }
} 

extern int* pwritten;
extern float* psteerCmd;
extern float* paccelCmd;
extern float* pbrakeCmd;
extern float* pangle;
extern float* pspeedX;
extern float* pspeedY;
extern float* pspeedZ;
extern float* pdist2middle;
extern float* ptrackSensorOut;
extern float* pwheelSpinVel;
extern float* prpm;
extern float* pdist2start;
extern int* pdamage;

/* Drive during race. */
static void  
drive(int index, tCarElt* car, tSituation *s) 
{ 
    memset((void *)&car->ctrl, 0, sizeof(tCarCtrl)); 

    //////////////////////////////////


//    while (*pwritten == 1)
//    {
//        usleep(1);
//    }

    
    // angle
    float angle = RtTrackSideTgAngleL(&(car->_trkPos)) - car->_yaw;
    NORM_PI_PI(angle);
    *pangle = angle;

    // dist_to_middle
    *pdist2middle = 2*car->_trkPos.toMiddle/(car->_trkPos.seg->width);

    // speedX, speedY, speedZ
    *pspeedX = float(car->_speed_x*3.6);
    *pspeedY = float(car->_speed_y*3.6);
    *pspeedZ = float(car->_speed_z*3.6);

    // wheelSpinVel
    for (int i=0; i<4; ++i)
    {
        pwheelSpinVel[i] = car->_wheelSpinVel(i);
    }

    // rpm
    *prpm = car->_enginerpm*10;

    // trackSensorOut
    if (*pdist2middle<=1.0 && *pdist2middle>=-1.0)
    {
        trackSens->sensors_update();
        for (int i=0; i<19; ++i)
        {
            ptrackSensorOut[i] = trackSens->getSensorOut(i);
        }
    }
    else
    {
        for (int i=0; i<19; ++i)
        {
            ptrackSensorOut[i] = -1;
        }
    }

    // dist2start
    *pdist2start = car->_distRaced;

    // damage
    *pdamage = car->_dammage;


    //printf("speed X=%f, Y=%f, Z=%f, accel=%f, sensor[0]=%f\n", *pspeedX, *pspeedY, *pspeedZ, *paccelCmd, ptrackSensorOut[0]);
    /*
    printf("sens[0]=%.2f,sens[1]=%.2f,sens[2]=%.2f,sens[3]=%.2f,sens[4]=%.2f,sens[5]=%.2f,sens[6]=%.2f,sens[7]=%.2f\n",
        ptrackSensorOut[0], ptrackSensorOut[1], ptrackSensorOut[2], ptrackSensorOut[3], ptrackSensorOut[4],ptrackSensorOut[5],ptrackSensorOut[6],ptrackSensorOut[7]);
    */

    // Contrl
    car->_steerCmd = *psteerCmd;
    car->_accelCmd = *paccelCmd;
    car->_brakeCmd = *pbrakeCmd;
    car->_gearCmd = 1;

//    printf(" *** steerCmd=%.2f ***\n", *psteerCmd);

    // auto
    /*
    //car->_steerCmd = (angle - (*pdist2middle)) / (car->_steerLock);
    car->_accelCmd = 0.4;
    car->_brakeCmd = 0.0;
    car->_gearCmd = 1;
    */

    // printf(" **** angle=%.2f, steerLock=%.2f, steer=%.2f **** \n", *pangle, car->_steerLock, car->_steerCmd);

    /*  auto gear
    if (*pspeedX > 50)
    {
       car->_gearCmd = 2;
    }
    if (*pspeedX > 80)
    {
       car->_gearCmd = 3;
    }
    if (*pspeedX > 110)
    {
        car->_gearCmd = 4;
    }
    if (*pspeedX > 140)
    {
        car->_gearCmd = 5;
    }
    if (*pspeedX > 170)
    {
        car->_gearCmd = 6;
    }
    */
   


//    *pwritten = 1;


    //////////////////////////////

    //car->ctrl.brakeCmd = 1.0; /* all brakes on ... */ 
    /*  
     * add the driving code here to modify the 
     * car->_steerCmd 
     * car->_accelCmd 
     * car->_brakeCmd 
     * car->_gearCmd 
     * car->_clutchCmd 
     */ 
}

/* End of the current race */
static void
endrace(int index, tCarElt *car, tSituation *s)
{
    if (trackSens != NULL)
    {
        delete trackSens;
        trackSens = NULL;
    }
}

/* Called before the module is unloaded */
static void
shutdown(int index)
{
    if (trackSens != NULL);
    {
        delete trackSens;
        trackSens = NULL;
    }
}

