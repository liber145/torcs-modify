#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <unistd.h>
#include <stdint.h>

#define image_width 640
#define image_height 480
#define span image_width*image_height

struct shared_use_st
{
    int written;
    uint8_t image[image_width*image_height*3];

    float steerCmd;
    float accelCmd;
    float brakeCmd;

    float angle;
    float speedX;
    float speedY;
    float speedZ;
    float dist2middle;
    float trackSensorOut[19];
    float wheelSpinVel[4];
    float rpm;
    float dist2start;
    int damage;
};

volatile int* pwritten;
uint8_t* pimage;
uint8_t* pimg;
volatile float* psteerCmd;
volatile float* paccelCmd;
volatile float* pbrakeCmd;
volatile float* pangle;
volatile float* pspeedX;
volatile float* pspeedY;
volatile float* pspeedZ;
volatile float* pdist2middle;
float* ptrackSensorOut;
float* pwheelSpinVel;
volatile float* prpm;
volatile float* pdist2start;
volatile int* pdamage;

extern "C"
{
      int connect()
      {
                
        int shmid = shmget((key_t)1234, sizeof(struct shared_use_st), 0666|IPC_CREAT);
        if (shmid == -1)
        {
            printf("shmget failed\n");
            exit(EXIT_FAILURE);
        }

        void* shm = shmat(shmid, 0, 0);
        if (shm == (void*)-1)
        {
            printf("shmat failed\n");
            exit(EXIT_FAILURE);
        }

        printf("*** Connected shmid at %d ***\n", shmid);

        struct shared_use_st* shared = (struct shared_use_st*) shm;
       
        pimage = shared->image; 
        pwritten = &shared->written;
        psteerCmd = &shared->steerCmd;
        paccelCmd = &shared->accelCmd;
        pbrakeCmd = &shared->brakeCmd;
        pangle = &shared->angle;
        pspeedX = &shared->speedX;
        pspeedY = &shared->speedY;
        pspeedZ = &shared->speedZ;
        pdist2middle = &shared->dist2middle;
        ptrackSensorOut = shared->trackSensorOut;
        pwheelSpinVel = shared->wheelSpinVel;
        prpm = &shared->rpm;
        pdist2start = &shared->dist2start;
        pdamage = &shared->damage;

        pimg = new uint8_t[image_width*image_height*3]; 

        return 0;
    }

    int disconnect()
    {
        int shmid = shmget((key_t)1234, sizeof(struct shared_use_st), 0666|IPC_CREAT);
        if (shmid == -1)
        {
            printf("shmget failed\n");
            exit(EXIT_FAILURE);
        }
        
        shmctl(shmid, IPC_RMID, NULL);
        printf("*** Disconnected shmid at %d ***\n\n", shmid);

        delete [] pimg;

        return 0;
    }

    int getwritten()
    {
        return *pwritten;
    }

    uint8_t* getimage()
    {
        
        int index = 0;
        for (int h=0; h<image_height; ++h)
        {
            for (int w=0; w<image_width; ++w)
            {
                pimg[index] = pimage[((image_height-1-h)*image_width+w)*3+0];
                pimg[index+span] = pimage[((image_height-1-h)*image_width+w)*3+1];
                pimg[index+2*span] = pimage[((image_height-1-h)*image_width+w)*3+2];
                ++index;
            }
        }
        return pimg;
    }

    float getangle()
    {
        return *pangle;
    }

    float getspeedX()
    {
        return *pspeedX;
    }

    float getspeedY()
    {
        return *pspeedY;
    }

    float getspeedZ()
    {
        return *pspeedZ;
    }

    float getdist2middle()
    {
        return *pdist2middle;
    }

    float* gettrackSensorOut()
    {
        return ptrackSensorOut;
    }

    float* getwheelSpinVel()
    {
        return pwheelSpinVel;
    }

    float getrpm()
    {
        return *prpm;
    }

    float getdist2start()
    {
        return *pdist2start;
    }

    int getdamage()
    {
        return *pdamage;
    }

    int setwritten(int written)
    {
        *pwritten = written;
        return 0;
    }

    int setsteerCmd(float steer)
    {
        *psteerCmd = steer;
        return 0;
    }

    int setaccelCmd(float accel)
    {
        *paccelCmd = accel;
        return 0;
    }

    int setbrakeCmd(float brake)
    {
        *pbrakeCmd = brake;
        return 0;
    }

}
