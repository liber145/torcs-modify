import ctypes
import time
import numpy as np
import os
from scipy import misc


class TorcsEnv(object):
    def __init__(self):
        self.isrgb = True
        self.imgwidth = 640
        self.imgheight = 480
        self.imreshape = (64, 64)
        self.torcslib = ctypes.CDLL('./ipcclient.so')
        self.torcslib.getwritten.restype = ctypes.c_int
        self.torcslib.getimage.restype = np.ctypeslib.ndpointer(dtype=ctypes.c_uint8, shape=(self.imgwidth*self.imgheight*3))
        self.torcslib.getangle.restype = ctypes.c_float
        self.torcslib.getspeedX.restype = ctypes.c_float
        self.torcslib.getspeedY.restype = ctypes.c_float
        self.torcslib.getspeedZ.restype = ctypes.c_float
        self.torcslib.getdist2middle.restype = ctypes.c_float
        self.torcslib.gettrackSensorOut.restype = np.ctypeslib.ndpointer(dtype=ctypes.c_float, shape=(19))
        self.torcslib.getwheelSpinVel.restype = np.ctypeslib.ndpointer(dtype=ctypes.c_float, shape=(4))
        self.torcslib.getrpm.restype = ctypes.c_float
        self.torcslib.getdist2start.restype = ctypes.c_float
        self.torcslib.getdamage.restype=ctypes.c_int

        self.observation = {'image':None, 'angle':None, 'speedX':None, 'speedY':None, 'speedZ':None, 
                'dist2middle':None, 'trackSensorOut':None, 'wheelSpinVel':None, 'rpm':None, 'dist2start':None, 'damage':None}

        self.old_reward = 0
        self.old_dist2start = 0
        self.cnt = 0

    def reset(self):
        self.cnt = 0
        self.stop()
        time.sleep(0.5)
        os.system('bash autostart.sh')
        time.sleep(0.5)

        isconnect = self.torcslib.connect()
        assert isconnect==0, '**** Failed to connect to torcs server. ****'

        self.make_observation()

        if self.isrgb:
            out = self.observation['image']
            out = np.array(misc.imresize(out, self.imreshape), dtype=np.float32)
            out /= 255.0
            out = np.asarray(np.dot(out, [0.299, 0.587, 0.114]).reshape((self.imreshape[0], self.imreshape[1], 1)), dtype=np.float32)
            print('**out.dtype:', out.dtype)
            print('out.max:', np.max(out))
            print('out.shape:', out.shape)
        else:
            out = np.hstack([self.observation['angle'], self.observation['speedX'], self.observation['speedY'], self.observation['speedZ'], self.observation['dist2middle'], self.observation['trackSensorOut'], self.observation['wheelSpinVel'], self.observation['rpm'], self.observation['dist2start']])

        # return self.observation
        return out

    
    def step(self, action):

        dam_last = self.torcslib.getdamage()

        steer = action[0]
        accel = action[1]
        brake = action[2]

        self.torcslib.setsteerCmd(ctypes.c_float(steer))
        self.torcslib.setaccelCmd(ctypes.c_float(accel))
        self.torcslib.setbrakeCmd(ctypes.c_float(brake))
        self.torcslib.setwritten(ctypes.c_int(0))

        while self.torcslib.getwritten() == 0:
            time.sleep(1e-6)

        done = False
        
        self.make_observation()

        sp = self.observation['speedX']
        ang = self.observation['angle'] * 3.14
        dis = self.observation['dist2middle']

        #reward = sp*np.cos(ang) - np.abs(sp*np.sin(ang)) - sp*np.abs(dis) - 1.0*sp * np.abs(self.old_steer - steer)
        reward = sp*np.cos(ang) 

        self.old_steer = steer

      
        # reward is -1 if no progress in 80 frames.
        if np.abs( self.old_dist2start - self.observation['dist2start'] ) < 0.2:
            self.cnt += 1
        else:
            self.cnt = 0

        if self.cnt > 80:
            reward = -1.0
            done = True

        self.old_dist2start = self.observation['dist2start']
        
        # reward is -1 if get damaged.
        dam = self.torcslib.getdamage()
        if dam > dam_last:
            reward = -1.0
            done = True


        if self.isrgb:
            out = self.observation['image']
            out = np.array(misc.imresize(out, self.imreshape), dtype=np.float32)
            out /= 255.0
            out = np.asarray(np.dot(out, [0.299, 0.587, 0.114]).reshape((self.imreshape[0], self.imreshape[1], 1)), dtype=np.float32)
        else:
            out = np.hstack([self.observation['angle'], self.observation['speedX'], self.observation['speedY'], self.observation['speedZ'], self.observation['dist2middle'], self.observation['trackSensorOut'], self.observation['wheelSpinVel'], self.observation['rpm'], self.observation['dist2start']])

        

        # return (newstate, reward, done, info)
        # return self.observation, reward, None, None
        return out, reward, done, None

        


    def make_observation(self):
        imagedata = self.torcslib.getimage()
        self.observation['image'] = np.stack([
                np.reshape(imagedata[:self.imgheight*self.imgwidth], (self.imgheight, self.imgwidth)),
                np.reshape(imagedata[self.imgheight*self.imgwidth:2*self.imgheight*self.imgwidth], (self.imgheight, self.imgwidth)),
                np.reshape(imagedata[2*self.imgheight*self.imgwidth:], (self.imgheight, self.imgwidth))], axis=2)
        self.observation['angle'] = self.torcslib.getangle() / 3.14
        self.observation['speedX'] = self.torcslib.getspeedX() / 200.0
        self.observation['speedY'] = self.torcslib.getspeedY() / 200.0
        self.observation['speedZ'] = self.torcslib.getspeedZ() / 200.0
        self.observation['dist2middle'] = self.torcslib.getdist2middle() 
        self.observation['trackSensorOut'] = self.torcslib.gettrackSensorOut() / 200.0
        self.observation['wheelSpinVel'] = self.torcslib.getwheelSpinVel() / 100.0
        self.observation['rpm'] = self.torcslib.getrpm() / 10000.0
        self.observation['dist2start'] = self.torcslib.getdist2start() 

    def stop(self):
        os.system('pkill torcs')
        self.torcslib.disconnect()

        

if __name__=='__main__':
    env = TorcsEnv()
    ob = env.reset()


    for i in range(600):
        # steer = ob['angle'] / 0.37 * 3.14
        steer = 0.05
        accel = 0.5
        brake = 0.0

#        time.sleep(0.5)
        ob, r, d, _ = env.step([steer, accel, brake])

        if d:
            ob = env.reset()


        # print(' angle={:.2f}, steer={:.2f} '.format(ob['angle'], steer))
        print(' reward={:.2f} '.format(r))

        if i % 10 == 0:
            misc.imsave("./image-{}.png".format(i), ob)
            
#        imagedata = np.array(ob['image'], dtype=np.uint8)
#        image = np.stack([np.reshape(imagedata[:self.imgheight*640], (480, 640)),
#                 np.reshape(imagedata[480*640:2*480*640], (480, 640)),
#                 np.reshape(imagedata[2*480*640:], (480, 640))], axis=2)

    
#        time.sleep(0.5)
        """
        image = np.zeros((480,640,3), dtype=np.uint8)
        image[:,:,0] = np.reshape(imagedata[:480*640], (480,640))
        image[:,:,1] = np.reshape(imagedata[480*640:480*640*2], (480,640))
        image[:,:,2] = np.reshape(imagedata[480*640*2:], (480,640))
        """

        """
        image = np.zeros((480, 640, 3), dtype=np.uint8)
        for h in range(480):
            for w in range(640):
                image[h,w,0] = imagedata[((480-1-h)*640+w)*3+0]
                image[h,w,1] = imagedata[((480-1-h)*640+w)*3+1]
                image[h,w,2] = imagedata[((480-1-h)*640+w)*3+2]
        """
        
#        print('image data shape:', imagedata.shape)
#        image = np.reshape(np.array(ob['image']), (640,480,3))
#        misc.imsave("./image-{}.png".format(i), image)

    env.stop()

"""
image_width=640
image_height=480

so = ctypes.cdll.LoadLibrary
lib = so('./ipcclient.so')
lib.getimage.restype = np.ctypeslib.ndpointer(dtype=ctypes.c_uint8, shape=(image_width*image_height*3))
lib.getangle.restype = ctypes.c_float
lib.getspeedX.restype = ctypes.c_float
lib.getspeedY.restype = ctypes.c_float
lib.getspeedZ.restype = ctypes.c_float
lib.getdist2middle.restype = ctypes.c_float
lib.gettrackSensorOut.restype = np.ctypeslib.ndpointer(dtype=ctypes.c_float, shape=(19))
lib.getwheelSpinVel.restype = np.ctypeslib.ndpointer(dtype=ctypes.c_float, shape=(4))
lib.getrpm.restype = ctypes.c_float

a = lib.connect()
print('a=', a)

for i in range(1000):

    while lib.getwritten() == 0:
        print("*****", i, "*****")
        time.sleep(1e-6)
    

    print('speedx:', float(lib.getspeedX()), 'speedy:', lib.getspeedY(), 'speedz:', lib.getspeedZ(), 
            'rpm:', lib.getrpm())

#    lib.setsteerCmd(0);
#    lib.setaccelCmd(ctypes.c_double(0.3));
#    lib.setbrakeCmd(ctypes.c_double(0.0));
    lib.setwritten(0);

"""
