import ctypes
import time
import numpy as np

image_width=640
image_height=480

so = ctypes.cdll.LoadLibrary
lib = so('./ipcclient.so')
lib.getimage.restype = np.ctypeslib.ndpointer(dtype=ctypes.c_uint8, shape=(image_width*image_height*3))
lib.getangle.restype = ctypes.c_float
lib.getspeedX.restype = ctypes.c_float
lib.getspeedY.restype = ctypes.c_float
lib.getspeedZ.restype = ctypes.c_float
lib.getdist2middle.restype = ctypes.c_float
lib.gettrackSensorOut.restype = np.ctypeslib.ndpointer(dtype=ctypes.c_float, shape=(19))
lib.getwheelSpinVel.restype = np.ctypeslib.ndpointer(dtype=ctypes.c_float, shape=(4))
lib.getrpm.restype = ctypes.c_float

a = lib.connect()
print('a=', a)


for i in range(1000):

    while lib.getwritten() == 0:
        print("*****", i, "*****")
        time.sleep(1e-6)
    

    print('speedx:', float(lib.getspeedX()), 'speedy:', lib.getspeedY(), 'speedz:', lib.getspeedZ(), 
            'rpm:', lib.getrpm())

#    lib.setsteerCmd(0);
#    lib.setaccelCmd(ctypes.c_double(0.3));
#    lib.setbrakeCmd(ctypes.c_double(0.0));
    lib.setwritten(0);


