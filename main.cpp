/***************************************************************************

    file                 : main.cpp
    created              : Sat Mar 18 23:54:30 CET 2000
    copyright            : (C) 2000 by Eric Espie
    email                : torcs@free.fr
    version              : $Id: main.cpp,v 1.14.2.3 2012/06/01 01:59:42 berniw Exp $

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdlib.h>

#include <GL/glut.h>

#include <tgfclient.h>
#include <client.h>

#include "linuxspec.h"
#include <raceinit.h>

#include <sys/shm.h>


#define image_width 640
#define image_height 480

extern bool bKeepModules;

static void
init_args(int argc, char **argv, const char **raceconfig)
{
	int i;
	char *buf;

	i = 1;

	while(i < argc) {
		if(strncmp(argv[i], "-l", 2) == 0) {
			i++;

			if(i < argc) {
				buf = (char *)malloc(strlen(argv[i]) + 2);
				sprintf(buf, "%s/", argv[i]);
				SetLocalDir(buf);
				free(buf);
				i++;
			}
		} else if(strncmp(argv[i], "-L", 2) == 0) {
			i++;

			if(i < argc) {
				buf = (char *)malloc(strlen(argv[i]) + 2);
				sprintf(buf, "%s/", argv[i]);
				SetLibDir(buf);
				free(buf);
				i++;
			}
		} else if(strncmp(argv[i], "-D", 2) == 0) {
			i++;

			if(i < argc) {
				buf = (char *)malloc(strlen(argv[i]) + 2);
				sprintf(buf, "%s/", argv[i]);
				SetDataDir(buf);
				free(buf);
				i++;
			}
		} else if(strncmp(argv[i], "-s", 2) == 0) {
			i++;
			SetSingleTextureMode();
		} else if(strncmp(argv[i], "-k", 2) == 0) {
			i++;
			// Keep modules in memory (for valgrind)
			printf("Unloading modules disabled, just intended for valgrind runs.\n");
			bKeepModules = true;
#ifndef FREEGLUT
		} else if(strncmp(argv[i], "-m", 2) == 0) {
			i++;
			GfuiMouseSetHWPresent(); /* allow the hardware cursor */
#endif
		} else if(strncmp(argv[i], "-r", 2) == 0) {
			i++;
			*raceconfig = "";

			if(i < argc) {
				*raceconfig = argv[i];
				i++;
			}

			if((strlen(*raceconfig) == 0) || (strstr(*raceconfig, ".xml") == 0)) {
				printf("Please specify a race configuration xml when using -r\n");
				exit(1);
			}
		} else {
			i++;		/* ignore bad args */
		}
	}

#ifdef FREEGLUT
	GfuiMouseSetHWPresent(); /* allow the hardware cursor (freeglut pb ?) */
#endif
}


struct shared_use_st
{
    int written;
    uint8_t image[image_width*image_height*3];

    float steerCmd;   // [-1,1]
    float accelCmd;   // [0,1]
    float brakeCmd;   // [0,1] 

    float angle;      // [-pi,pi]
    float speedX;     // km/h   50
    float speedY;     // km/h   0
    float speedZ;     // km/h   0
    float dist2middle;        // [-1,1] inside lane
    float trackSensorOut[19]; // [0,200] 
    float wheelSpinVel[4];    // rad/s 
    float rpm;                // number of rotation per minute of engine.
    float dist2start;
    int damage;
};

volatile uint8_t* pimage;
volatile int* pwritten_ghost;
volatile float* psteerCmd_ghost;
volatile float* paccelCmd_ghost;
volatile float* pbrakeCmd_ghost;
volatile float* pangle_ghost;
volatile float* pspeedX_ghost;
volatile float* pspeedY_ghost;
volatile float* pspeedZ_ghost;
volatile float* pdist2middle_ghost;
volatile float* ptrackSensorOut_ghost;
volatile float* pwheelSpinVel_ghost;
volatile float* prpm_ghost;
volatile float* pdist2start_ghost;
volatile int* pdamage_ghost;

/*
 * Function
 *	main
 *
 * Description
 *	LINUX entry point of TORCS
 *
 * Parameters
 *
 *
 * Return
 *
 *
 * Remarks
 *
 */
int
main(int argc, char *argv[])
{

    int shmid = shmget((key_t)1234, sizeof(struct shared_use_st), 0666|IPC_CREAT);
    if (shmid == -1)
    {
        printf("shmget failed\n");
        exit(EXIT_FAILURE);
    }

    void* shm = shmat(shmid, 0, 0);
    if (shm == (void*)-1)
    {
        printf("shmat failed\n");
        exit(EXIT_FAILURE);
    }
    printf(" **** Start memory sharing, shmid = %d **** \n\n", shmid);


    struct shared_use_st* shared = (struct shared_use_st*) shm;
    shared->written = 0;
    shared->steerCmd = 0;
    shared->accelCmd = 0.3;
    shared->brakeCmd = 0.0;

    pimage = shared->image;
    pwritten_ghost = &shared->written;
    psteerCmd_ghost = &shared->steerCmd;
    paccelCmd_ghost = &shared->accelCmd;
    pbrakeCmd_ghost = &shared->brakeCmd;

    pangle_ghost = &shared->angle;
    pspeedX_ghost = &shared->speedX;
    pspeedY_ghost = &shared->speedY;
    pspeedZ_ghost = &shared->speedZ;
    pdist2middle_ghost = &shared->dist2middle;
    ptrackSensorOut_ghost = shared->trackSensorOut;
    pwheelSpinVel_ghost = shared->wheelSpinVel;
    prpm_ghost = &shared->rpm;
    pdist2start_ghost =  &shared->dist2start;
    pdamage_ghost = &shared->damage;



    ///////////////////////

	const char *raceconfig = "";

	init_args(argc, argv, &raceconfig);
	LinuxSpecInit();			/* init specific linux functions */

	if(strlen(raceconfig) == 0) {
		GfScrInit(argc, argv);	/* init screen */
		TorcsEntry();			/* launch TORCS */
		glutMainLoop();			/* event loop of glut */
	} else {
		// Run race from console, no Window, no OpenGL/OpenAL etc.
		// Thought for blind scripted AI training
		ReRunRaceOnConsole(raceconfig);
	}

	return 0;					/* just for the compiler, never reached */
}

